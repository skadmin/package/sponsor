<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220623174447 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sponsor_type ADD sequence INT NOT NULL');

        $translations = [
            ['original' => 'grid.sponsor.overview-type.action.flash.sort.success', 'hash' => '306a93d81323c313e6c28738eca3aabe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí typů sponzorů bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.action.flash.sort.success', 'hash' => 'c661a78da8ab7261694a14802d6fe240', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí sponzorů bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sponsor_type DROP sequence');
    }
}
