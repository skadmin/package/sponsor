<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901173808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'grid.sponsor.overview.is-active', 'hash' => '986837c5009ad0c290045f8e1ab4b562', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.change-webalize', 'hash' => '5f2a0b15ec5d4eea8b05178803c26dff', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přegenerovat url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.description', 'hash' => '457da8388a683bf1c74f395539c8f0eb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.is-hp', 'hash' => 'eb7438017da1a9f57acf62de7cec4052', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit na HP', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.is-detail', 'hash' => '542457b3faacb2aa5a3014136743389f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit detail', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
