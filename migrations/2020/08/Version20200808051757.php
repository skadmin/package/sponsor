<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200808051757 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'sponsor.overview', 'hash' => 'c74829856faa14d66bbf38a20a23bb05', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Sponzoři', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor.overview.title', 'hash' => 'f4809df785ad9a1c4df04c07944c28ed', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Sponzoři|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.sponsor-type', 'hash' => 'fb902e282033381aee43a7ebe9c81126', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.action.sponsor-type', 'hash' => 'b74d5e303ea805e19bb591ae2072f973', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled typů sponzorů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.action.new', 'hash' => 'c496e357a00cdaebf10e0f42f9becd0c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.name', 'hash' => 'f4d47f04ce1619de8e9d2e37381de74c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.website', 'hash' => 'c01febcb80327ca74d6c5c16af4cc032', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Webové stránky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.action.edit', 'hash' => '3923faf896f4b408e1404aecacc1a92b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor.overview-type.title', 'hash' => '4a84e84bfff677c569091f7238fbd66a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy sponzorů|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview.action.sponsor', 'hash' => '4fcc2c54a61a8ef1717ac8d6a85c32a6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled sponzorů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview-type.action.new', 'hash' => '9719baf3e052bee187f95e1baa14546e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit typ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview-type.name', 'hash' => '3c7b1bf785b2ad45c5807126e1b53c00', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.sponsor.overview-type.action.edit', 'hash' => 'cd53f6752162a2f69881d4407d1ac714', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor-type.edit.title', 'hash' => '2ea685752adc06d31ac83dd6202eb196', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení typu sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.name', 'hash' => 'd97e31d857cfc009c0366a936641d8eb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.name.req', 'hash' => '31565764b870bc0ad37253a91f57681c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.content', 'hash' => '6799cf6e6cd8bb7badb40ace6fbff201', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.send', 'hash' => '48fb790bbc7ad6c9b5f230c5f97ebb11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.send-back', 'hash' => 'c020fb6cafe1486b1dd2975d50495960', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.back', 'hash' => 'f2c89ece4850b8274b6f45f1988593d2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor-type.edit.title - %s', 'hash' => 'ef260c237d486b98942392e1e6730ccd', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace typu sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.flash.success.create', 'hash' => 'ead1f03392186559cb295a5cbd4a4a0b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ sponzora byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor-type.edit.flash.success.update', 'hash' => '4a593fcba73096ed5901ba0ae6a4a348', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ sponzora byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor.edit.title', 'hash' => 'd1bbad1193412bf8ac1fa141c2325b97', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.sponsor-type-id', 'hash' => '96059ada3e7b4d014e054812fdae9044', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.sponsor-type-id.req', 'hash' => '8240d2e31e093f75b6c735282ab2fe11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím typ sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.name', 'hash' => '88497ef1ccbf3279e3ea375e1e564aa3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.name.req', 'hash' => '732a032895fbff14341b1dd1d26ee5b3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.website', 'hash' => '39eccae72afb39305654295d1ee9daae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Webové stránky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.image-preview', 'hash' => '9f83c60be0da944ef2a2b7a9ba2f931d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.image-preview.rule-image', 'hash' => '62e4496157dea7d2508faaecbbb010bf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.content', 'hash' => 'b3b2659047add6b0a2861bae815615dc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.is-active', 'hash' => '67a2eb98025c96d6c93366a7bd33cff2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.send', 'hash' => '79d5e828279497659765b2ba9b149803', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.send-back', 'hash' => 'fa4e606d53d4c17462254bf9963522a4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.back', 'hash' => '5588433cf4349c76e4395a1c82190e79', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'sponsor.edit.title - %s', 'hash' => 'eb3eafdcd8334c8f09f271f931a23c37', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace sponzora', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.flash.success.create', 'hash' => 'dadd0b71146523c90254e9aaa1480eaa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Sponzor byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.sponsor.edit.flash.success.update', 'hash' => 'd17235ad2b183fd0236b1ce8a974ab10', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Sponzor byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.sponsor.title', 'hash' => '5029f702d2869b76840b77dff59d6d33', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Sponzoři', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.sponsor.description', 'hash' => '5e330330eb788f943a15415f0a983486', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat sponzory', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
