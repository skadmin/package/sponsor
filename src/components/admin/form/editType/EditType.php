<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Sponsor\BaseControl;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorType;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory     $webLoader;
    private SponsorTypeFacade $facade;
    private SponsorType       $sponsorType;

    public function __construct(?int $id, SponsorTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->sponsorType = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->sponsorType->isLoaded()) {
            return new SimpleTranslation('sponsor-type.edit.title - %s', $this->sponsorType->getName());
        }

        return 'sponsor-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->sponsorType->isLoaded()) {
            $sponsorType = $this->facade->update(
                $this->sponsorType->getId(),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.sponsor-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $sponsorType = $this->facade->create(
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.sponsor-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $sponsorType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.sponsor-type.edit.name')
            ->setRequired('form.sponsor-type.edit.name.req');
        $form->addTextArea('content', 'form.sponsor-type.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.sponsor-type.edit.send');
        $form->addSubmit('sendBack', 'form.sponsor-type.edit.send-back');
        $form->addSubmit('back', 'form.sponsor-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->sponsorType->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->sponsorType->getName(),
            'content' => $this->sponsorType->getContent(),
        ];
    }
}
