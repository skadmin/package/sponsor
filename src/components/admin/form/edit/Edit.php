<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Sponsor\BaseControl;
use Skadmin\Sponsor\Doctrine\Sponsor\Sponsor;
use Skadmin\Sponsor\Doctrine\Sponsor\SponsorFacade;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory     $webLoader;
    private SponsorFacade     $facade;
    private SponsorTypeFacade $facadeSponsorType;
    private Sponsor           $sponsor;
    private ImageStorage      $imageStorage;

    public function __construct(?int $id, SponsorFacade $facade, SponsorTypeFacade $facadeSponsorType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeSponsorType = $facadeSponsorType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->sponsor = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->sponsor->isLoaded()) {
            return new SimpleTranslation('sponsor.edit.title - %s', $this->sponsor->getName());
        }

        return 'sponsor.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->sponsor->isLoaded()) {
            if ($identifier !== null && $this->sponsor->getImagePreview() !== null) {
                $this->imageStorage->delete($this->sponsor->getImagePreview());
            }

            $sponsor = $this->facade->update(
                $this->sponsor->getId(),
                $values->name,
                $values->changeWebalize,
                $values->content,
                $values->description,
                $values->isActive,
                $values->isHp,
                $values->isDetail,
                $values->website,
                $this->facadeSponsorType->get($values->sponsorTypeId),
                $identifier
            );
            $this->onFlashmessage('form.sponsor.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $sponsor = $this->facade->create(
                $values->name,
                $values->content,
                $values->description,
                $values->isActive,
                $values->isHp,
                $values->isDetail,
                $values->website,
                $this->facadeSponsorType->get($values->sponsorTypeId),
                $identifier
            );
            $this->onFlashmessage('form.sponsor.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $sponsor->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->sponsor = $this->sponsor;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataSponsorsType = $this->facadeSponsorType->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.sponsor.edit.name')
            ->setRequired('form.sponsor.edit.name.req');
        $form->addText('website', 'form.sponsor.edit.website');
        $form->addCheckbox('isActive', 'form.sponsor.edit.is-active')
            ->setDefaultValue(true);
        $form->addCheckbox('isHp', 'form.sponsor.edit.is-hp')
            ->setDefaultValue(true);
        $form->addCheckbox('isDetail', 'form.sponsor.edit.is-detail')
            ->setDefaultValue(true)
            ->addCondition(Form::EQUAL, true)
            ->toggle('form-content');
        $form->addImageWithRFM('imagePreview', 'form.sponsor.edit.image-preview');

        $form->addSelect('sponsorTypeId', 'form.sponsor.edit.sponsor-type-id', $dataSponsorsType)
            ->setRequired('form.sponsor.edit.sponsor-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        if ($this->sponsor->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.sponsor.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // TEXT
        $form->addTextArea('description', 'form.sponsor.edit.description');
        $form->addTextArea('content', 'form.sponsor.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.sponsor.edit.send');
        $form->addSubmit('sendBack', 'form.sponsor.edit.send-back');
        $form->addSubmit('back', 'form.sponsor.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->sponsor->isLoaded()) {
            return [];
        }

        return [
            'name'          => $this->sponsor->getName(),
            'content'       => $this->sponsor->getContent(),
            'description'   => $this->sponsor->getDescription(),
            'isActive'      => $this->sponsor->isActive(),
            'isHp'          => $this->sponsor->isHp(),
            'isDetail'      => $this->sponsor->isDetail(),
            'website'       => $this->sponsor->getWebsite(),
            'sponsorTypeId' => $this->sponsor->getSponsorType()->getId(),
        ];
    }
}
