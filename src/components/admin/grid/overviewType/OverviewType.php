<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Sponsor\BaseControl;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewType extends GridControl
{
    use APackageControl;

    private SponsorTypeFacade $facade;
    private LoaderFactory     $webLoader;

    public function __construct(SponsorTypeFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;

        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewType.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'sponsor.overview-type.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelSorted(sortProperty: 'a.sequence'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.sponsor.overview-type.name');

        // FILTER
        $grid->addFilterText('name', 'grid.sponsor.overview-type.name', ['name', 'code']);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.sponsor.overview-type.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.sponsor.overview.action.sponsor', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('handshake')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.sponsor.overview-type.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-type',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.sponsor.overview-type.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
