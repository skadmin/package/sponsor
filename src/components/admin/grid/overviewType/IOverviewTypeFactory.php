<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
