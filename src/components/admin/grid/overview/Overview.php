<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Sponsor\BaseControl;
use Skadmin\Sponsor\Doctrine\Sponsor\Sponsor;
use Skadmin\Sponsor\Doctrine\Sponsor\SponsorFacade;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private SponsorFacade     $facade;
    private SponsorTypeFacade $facadeSponsorType;
    private LoaderFactory     $webLoader;

    public function __construct(SponsorFacade $facade, SponsorTypeFacade $facadeSponsorType, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade            = $facade;
        $this->facadeSponsorType = $facadeSponsorType;

        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'sponsor.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataSponsorsType = $this->facadeSponsorType->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->leftJoin('a.sponsorType', 'b')
            ->orderBy('b.sequence', 'ASC')
            ->addOrderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.sponsor.overview.name')
            ->setRenderer(function (Sponsor $sponsor): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $sponsor->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($sponsor->getName());

                return $name;
            });
        $grid->addColumnText('sponsorType', 'grid.sponsor.overview.sponsor-type', 'sponsorType.name');
        $grid->addColumnText('website', 'grid.sponsor.overview.website')
            ->setRenderer(static function (Sponsor $sponsor): ?Html {
                if ($sponsor->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $sponsor->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($sponsor->getWebsite());
            });
        $this->addColumnIsActive($grid, 'sponsor.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.sponsor.overview.name', ['name']);
        $grid->addFilterSelect('sponsorType', 'grid.sponsor.overview.sponsor-type', $dataSponsorsType, 'sponsorType')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'sponsor.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.sponsor.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.sponsor.overview.action.sponsor-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.sponsor.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // OTHER
        $grid->setDefaultFilter(['isActive' => 1]);

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $sponsor = $this->facade->get(intval($itemId));
        $this->facade->sort($itemId, $prevId, $nextId, ['sponsor_type_id = %d' => $sponsor->getSponsorType()->getId()]);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.sponsor.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
