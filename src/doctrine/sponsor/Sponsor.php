<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Doctrine\Sponsor;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorType;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Sponsor
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Description;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Website;

    #[ORM\Column(options: ['default' => true])]
    private bool $isHp = true;

    #[ORM\Column(options: ['default' => true])]
    private bool $isDetail = true;

    #[ORM\ManyToOne(targetEntity: SponsorType::class, inversedBy: 'sponsors')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private SponsorType $sponsorType;

    public function update(string $name, string $content, string $description, bool $isActive, bool $isHp, bool $isDetail, string $website, SponsorType $sponsorType, ?string $imagePreview): void
    {
        $this->name        = $name;
        $this->content     = $content;
        $this->description = $description;
        $this->isHp        = $isHp;
        $this->isDetail    = $isDetail;
        $this->website     = $website;

        $this->sponsorType = $sponsorType;

        $this->setIsActive($isActive);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }

    public function isHp(): bool
    {
        return $this->isHp;
    }

    public function isDetail(): bool
    {
        return $this->isDetail;
    }

    public function getSponsorType(): SponsorType
    {
        return $this->sponsorType;
    }
}
