<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Doctrine\Sponsor;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Sponsor\Doctrine\SponsorType\SponsorType;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class SponsorFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Sponsor::class;
    }

    public function create(string $name, string $content, string $description, bool $isActive, bool $isHp, bool $isDetail, string $website, SponsorType $sponsorType, ?string $imagePreview): Sponsor
    {
        return $this->update(null, $name, false, $content, $description, $isActive, $isHp, $isDetail, $website, $sponsorType, $imagePreview);
    }

    public function update(?int $id, string $name, bool $changeWebalize, string $content, string $description, bool $isActive, bool $isHp, bool $isDetail, string $website, SponsorType $sponsorType, ?string $imagePreview): Sponsor
    {
        $sponsor = $this->get($id);
        $sponsor->update($name, $content, $description, $isActive, $isHp, $isDetail, $website, $sponsorType, $imagePreview);

        if (! $sponsor->isLoaded()) {
            $sponsor->setSequence($this->getValidSequence());
            $sponsor->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $sponsor->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($sponsor);
        $this->em->flush();

        return $sponsor;
    }

    public function get(?int $id = null): Sponsor
    {
        if ($id === null) {
            return new Sponsor();
        }

        $sponsor = parent::get($id);

        if ($sponsor === null) {
            return new Sponsor();
        }

        return $sponsor;
    }

    /**
     * @return Sponsor[]
     */
    public function getAll(bool $onlyActive = false, ?bool $isHp = null, ?bool $isDetail = null): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        if ($isHp !== null) {
            $criteria['isHp'] = $isHp;
        }

        if ($isDetail !== null) {
            $criteria['isDetail'] = $isDetail;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?Sponsor
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
