<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Doctrine\SponsorType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class SponsorTypeFacade extends Facade
{
    use Facade\Sequence;
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = SponsorType::class;
    }

    public function create(string $name, string $content): SponsorType
    {
        return $this->update(null, $name, $content);
    }

    public function update(?int $id, string $name, string $content): SponsorType
    {
        $sponsorType = $this->get($id);
        $sponsorType->update($name, $content);

        if (! $sponsorType->isLoaded()) {
            $sponsorType->setSequence($this->getValidSequence());
            $sponsorType->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($sponsorType);
        $this->em->flush();

        return $sponsorType;
    }

    public function get(?int $id = null): SponsorType
    {
        if ($id === null) {
            return new SponsorType();
        }

        $sponsorType = parent::get($id);

        if ($sponsorType === null) {
            return new SponsorType();
        }

        return $sponsorType;
    }

    /**
     * @return SponsorType[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findBy([], ['sequence' => 'ASC']);
    }
}
