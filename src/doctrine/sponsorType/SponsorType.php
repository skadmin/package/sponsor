<?php

declare(strict_types=1);

namespace Skadmin\Sponsor\Doctrine\SponsorType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Sponsor\Doctrine\Sponsor\Sponsor;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SponsorType
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    /** @var ArrayCollection|Collection|Sponsor[] */
    #[ORM\OneToMany(targetEntity: Sponsor::class, mappedBy: 'sponsorType')]
    private ArrayCollection|Collection|array $sponsors;

    public function __construct()
    {
        $this->sponsors = new ArrayCollection();
    }

    public function update(string $name, string $content): void
    {
        $this->name    = $name;
        $this->content = $content;
    }

    /**
     * @return ArrayCollection|Collection|Sponsor[]
     */
    public function getSponsors(bool $onlyActive = false, ?bool $isHp = null, ?bool $isDetail = null): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        if ($isHp !== null) {
            $criteria->andWhere(Criteria::expr()->eq('isHp', $isHp));
        }

        if ($isDetail !== null) {
            $criteria->andWhere(Criteria::expr()->eq('isDetail', $isDetail));
        }

        return $this->sponsors->matching($criteria);
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdateWebalizeName(): void
    {
    }
}
